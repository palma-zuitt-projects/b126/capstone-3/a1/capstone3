//import dependencies

const express = require("express")
const mongoose = require("mongoose")

//import routes

const userRoutes = require("./routes/user")
const productRoutes =require("./routes/product")
const orderRoutes = require("./routes/order")
const cors = require("cors")
//add database connection 

mongoose.connect("mongodb+srv://admin:admin@cluster0.knygy.mongodb.net/b126_onlineshopping_capstone2?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology: true
})

//confirm Atlas connection is console
mongoose.connection.once('open',() => console.log ('Now connected to mongoDB Atlas'))


//server setup
const app = express()
app.use(cors())
app.use(express.json()) //allows your server to read and send json data
app.use(express.urlencoded({
	extended:true
})); // allows your server to read data from forms

//add routes
app.use("/users", userRoutes)
app.use("/products", productRoutes)
app.use("/orders", orderRoutes)



const port = 4000

//listen to the server

app.listen(process.env.PORT || port, () => {
	console.log(`Server running at ${port}`)
})
