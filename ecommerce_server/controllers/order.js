const Order = require ("../models/order");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//create a new order

module.exports.placeOrder = (body) =>{
	let newOrder = new Order({
		userId : body.userId,
		products : body.products,
		totalAmount : body.totalAmount
	})

	return newOrder.save().then((order,error) => {
		if(error){
			return false; // order was NOT saved
		}else{
			return true; // order was successfully saved
		}
	})
}



//get the currently logged user order

module.exports.userOrderList = (orderData) => {
	return Order.find({userId:orderData.id}).then(result =>{
		return result;
	})
}




//Get all orders ADMIN only

module.exports.getOrders = () => {
	return Order.find({}).then(result => {
		return result; 
	})
}