const Product = require ("../models/product");

//create a new product

module.exports.addProduct =(body) => {
	let newProduct = new Product({
		name: body.name,
		description: body.description,
		price : body.price,
	})

	return newProduct.save().then((product,error) => {
		if(error){
			return false; // user was NOT saved
		}else{
			return true; // user was successfully saved
		}
	})
}

//get all active products
module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
}



//get all products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}



//get specific product
module.exports.getSpecificProduct = (params) => {
	return Product.findById(params.productId).then(result => {
		return result;
	})
}

//update specific product information

module.exports.updateSpecificProduct = (params, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		price: body.price,
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, err) => {

		//error handling
		if(err) {
			return false;
		}else{
			return true;
		}
	})
}

//archived specific product

module.exports.archiveSpecificProduct = (params, body) => {
	let archivedProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params.productId, archivedProduct).then((product, err) => {

		//error handling
		if(err) {
			return false;
		}else{
			return true;
		}
	})
}
