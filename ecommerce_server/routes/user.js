//import dependencies

const express = require("express");

const router = express.Router();

const userController = require("../controllers/user");

const auth = require("../auth");

//Router for registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

//Router for checking duplicate emails
router.post("/checkEmail", (req,res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send (resultFromController))
})

//Router for logging in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (
		resultFromController))
})

//Router for getting user details

router.get("/profile", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.userOrderList(userData).then(resultFromController => res.send (
		resultFromController))
})





module.exports = router;